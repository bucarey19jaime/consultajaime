package com.workshop.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class EspecialidadMedica {
	public Integer getIdEspecialidadMedica() {
		return idEspecialidadMedica;
	}
	public void setIdEspecialidadMedica(Integer idEspecialidadMedica) {
		this.idEspecialidadMedica = idEspecialidadMedica;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idEspecialidadMedica;
	@Column(name="descripcion",length = 200,nullable=false)
	private String descripcion;
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
