package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workshop.models.Usuario;

public interface IUsuarioDAO extends JpaRepository<Usuario, Integer> {
		
	Usuario findOneByUsername(String username);
}