package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workshop.models.Medico;



public interface IMedicoDao extends JpaRepository<Medico, Integer> {

}
