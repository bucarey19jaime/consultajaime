package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workshop.models.Consulta;
@Repository
public interface IConsultaDAO extends JpaRepository<Consulta, Integer>{

}
