package com.workshop.service;

import java.util.List;

import com.workshop.models.Consulta;

public interface IConsultaService {
	Consulta persist(Consulta e);
	List<Consulta> getAll();
	Consulta findById(Integer id);
	Consulta merge(Consulta e);
	void delete(Integer id);

}