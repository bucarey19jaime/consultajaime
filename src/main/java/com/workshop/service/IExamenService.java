package com.workshop.service;

import java.util.List;

import com.workshop.models.Examen;

public interface IExamenService {

	Examen persist(Examen e);
	List<Examen> getAll();
	Examen findById(Integer id);
	Examen merge(Examen e);
	void delete(Integer id);
}
