package com.workshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultaJaimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaJaimeApplication.class, args);
	}

}
